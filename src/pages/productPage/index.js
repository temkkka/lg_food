import React from "react";
import Footer from "../../component/footer";
import NavBar from "../../component/Navbar";
import ProductCard from "../../component/productCard";
import css from "./style.module.css";
const ProductPage = () => {
  return (
    <div className={css.Container}>
      <div className={css.navbar}>
        <NavBar />
      </div>
      <div className={css.category}>
        <div className={css.text}>Бүгд</div>
        <div className={css.text}>Солонгос хоол</div>
        <div className={css.text}>Хоолын хачир</div>
        <div className={css.empty}>a</div>
        <div className={css.empty}>a</div>
      </div>
      <div className={css.cards}>
        <div className={css.innercard}>
          <ProductCard />
        </div>
        <div className={css.innercard}>
          <ProductCard />
        </div>
        <div className={css.innercard}>
          <ProductCard />
        </div>
      </div>
      <div className={css.foot}>
        <Footer />
      </div>
    </div>
  );
};
export default ProductPage;
