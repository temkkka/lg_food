import React from "react";
import css from "./style.module.css";
import Navbar from "../../component/Navbar";
import Footer from "../../component/footer";
import { MdKeyboardArrowRight } from "react-icons/md";
import OrderCard from "../../component/orderCard";

const OrderPage = () => {
  return (
    <>
      <div className={css.container}>
        <div className={css.navBar}>
          <Navbar />
        </div>
        <div className={css.mainContainer}>
          <div className={css.text}> Захиалсан бүтээгдэхүүн </div>
          <div className={css.orderCard}>
            <OrderCard />
          </div>
          <div className={css.orderCard}>
            <OrderCard />
          </div>
          <div className={css.orderCard}>
            <OrderCard />
          </div>
          <div className={css.inActiveText}>
            Хүргэлтийн мэдээлэл <MdKeyboardArrowRight className={css.arrow} />
          </div>
          <div className={css.inActiveText}>
            Төлбөрийн мэдээлэл <MdKeyboardArrowRight className={css.arrow} />
          </div>
          <div className={css.buttonContainer}>
            <button className={css.button}> Захиалгийн хүсэлт илгээх</button>
          </div>
        </div>
        <div className={css.footer}>
          <Footer />
        </div>
      </div>
    </>
  );
};
export default OrderPage;
