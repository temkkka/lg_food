import React from "react";
import css from "./style.module.css";
import Heart from "../../Assets/heart-shape-silhouette.svg";
import Home from "../../Assets/home.svg";
import Pin from "../../Assets/pin.svg";
import Shop from "../../Assets/shopping-cart.svg";
const Footer = () => {
  return (
    <div className={css.Container}>
      <div className={css.back}>
        <img className={css.image} src={Heart} alt="heart" />
      </div>
      <div className={css.back}>
        <img className={css.image} src={Home} alt="home" />
      </div>
      <div className={css.back}></div>

      <div className={css.back}>
        <img className={css.image} src={Pin} alt="" />
      </div>
      <div className={css.back}>
        <img className={css.image} src={Shop} alt="" />
      </div>
    </div>
  );
};
export default Footer;
