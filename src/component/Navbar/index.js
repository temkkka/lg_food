import React from "react";
import css from "./style.module.css";
import Logo from "../../Assets/menu-button.svg";
import { GoSearch } from "react-icons/go";
const NavBar = () => {
  return (
    <div className={css.Container}>
      <div className={css.empty}>
        <div className={css.image}>
          <GoSearch />
        </div>
      </div>
      <div className={css.text}>ЛЖ ХҮНС</div>
      <div className={css.logo}>
        <img className={css.image} src={Logo} alt="tmke" />
      </div>
    </div>
  );
};

export default NavBar;
