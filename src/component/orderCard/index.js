import React, { useState } from "react";
import css from "./style.module.css";
import Img from "../../Assets/sam.jpg";
import { BiMinus } from "react-icons/bi";
import { BiPlus } from "react-icons/bi";

const OrderCard = () => {
  let Initial = 3000;
  const [amount, setAmount] = useState(() => {
    return 1;
  });
  const [total, setTotal] = useState(() => {
    return Initial * amount;
  });

  function minusAmount() {
    if (amount > 0) {
      setAmount((prevAmount) => prevAmount - 1);
    } else {
      setAmount((prevAmount) => prevAmount - 0);
    }
    if (total > 0) {
      setTotal((prevTotal) => prevTotal - Initial);
    } else {
      setTotal((prevTotal) => prevTotal - 0);
    }
  }
  function plusAmount() {
    setAmount((prevAmount) => prevAmount + 1);
    setTotal((prevTotal) => prevTotal + Initial);
  }
  return (
    <div className={css.container}>
      <img src={Img} className={css.image} alt="" />
      <div className={css.rightContainer}>
        <div className={css.firstLine}>
          <div className={css.name}>Самгёбсаль</div>
          <div className={css.price}> {Initial} ₮</div>
        </div>
        <div className={css.secondLine}>
          <div className={css.minus} onClick={minusAmount}>
            <BiMinus className={css.minusIcon} />
          </div>
          <div className={css.quantity}>{amount}</div>
          <div className={css.add} onClick={plusAmount}>
            <BiPlus className={css.plusIcon} />
          </div>
        </div>
        <div className={css.lastLine}>
          <div className={css.text}> Нийт үнэ : </div>
          <div className={css.totalPrice}> {total} </div>
        </div>
      </div>
    </div>
  );
};
export default OrderCard;
