import React from "react";
import css from "./style.module.css";
import Img from "../../Assets/sam.jpg";
import { AiFillHeart } from "react-icons/ai";
import { BiCommentDots } from "react-icons/bi";
import { MdShoppingCart } from "react-icons/md";
const ProductCard = () => {
  return (
    <div className={css.Container}>
      <div className={css.image}>
        <img className={css.img} src={Img} alt="" />
      </div>
      <div className={css.text}>
        <div>Samgyobsal</div>
        <div className={css.head}>
          It is a long established fact that a reader will be distracted by the
          readable content of a page when looking at its layout. The point of
          using Lorem Ipsum is that it has a more-or-less normal distribution of
          letters
        </div>
        <div className={css.price}>
          <div className={css.logo}>
            <AiFillHeart /> 8
            <BiCommentDots /> 8
          </div>

          <div>price: 3000</div>
        </div>
      </div>
      <div className={css.icon}>
        <div className={css.innerIcon}>
          <MdShoppingCart />
        </div>
      </div>
    </div>
  );
};
export default ProductCard;
